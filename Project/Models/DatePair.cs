﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project.Models {
	public class DatePair {

		[Required]
		public string Start { get; set; }

		[Required]
		public string End { get; set; }

		public DatePair() {
			Start = "";
			End = "";
		}
	}
}
