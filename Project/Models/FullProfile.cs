﻿using System;
using System.Collections.Generic;

using Project.Data;

namespace Project.Models {
	public class FullProfile {

		public User user { get; set; }
		public List<Appointment> appointments { get; set; }
		public List<Activity> activities { get; set; }

		public FullProfile() {
		}
	}
}
