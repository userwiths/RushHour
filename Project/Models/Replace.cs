﻿using System;
using System.Collections.Generic;

namespace Project.Models {
	public class ReplaceActivity {

		public int OldId { get; set; }
		public Data.Activity NewActivity{get;set;}

		public ReplaceActivity() {
		}
	}


	public class ReplaceAppointment {

		public int OldId { get; set; }
		public DatePair pair { get; set; }
		public SelectActivity selected { get; set; }

		public ReplaceAppointment() {
			pair = new DatePair();
			selected = new SelectActivity();
		}

		public ReplaceAppointment(int id,List<Data.Activity> act) {
			pair = new DatePair();
			selected = new SelectActivity();
			selected.activity = act;
			foreach (var item in act) {
				selected.selected.Add(false);
			}
			OldId = id;
		}
	}
}
