﻿using System;
using System.Collections.Generic;

namespace Project.Models {
	public class SmartAppointment{
		public int ID{get;set;}
		public int UserId{get;set;}
		public string Start{get;set;}
		public string End{get;set;}
		public List<Data.Activity> activity { get; set; }

		public SmartAppointment(Data.Appointment ap,List<Data.Activity> act){
			
			this.ID = ap.ID;
			this.UserId = ap.UserId;
			this.Start = ap.Start.ToString("dd/mm/yyyy");
			this.End = ap.End.ToString("dd/mm/yyyy");
			this.activity = act;
		}

		public SmartAppointment() {
			activity = new List<Data.Activity>();
		}
	}
}
