﻿using System;
using System.Collections.Generic;

namespace Project.Models {
	public class FullAppointment {

		public DatePair pair { get; set; }
		public SelectActivity selected { get; set; }

		public FullAppointment() {
			pair = new DatePair();
			selected = new SelectActivity();
		}
	}
}
