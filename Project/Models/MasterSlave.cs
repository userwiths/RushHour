﻿using System;
using System.Collections.Generic;

namespace Project.Models {
	public class MasterSlave {

		public Data.User master;
		public List<Data.User> slaves;

		public MasterSlave() {
			master = new Data.User();
			slaves = new List<Data.User>();
		}
	}
}
