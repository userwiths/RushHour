using System;

namespace Project.Models{
    public class SearchOption{
        public int ItemsPerPage{get;set;}
        public string Filter{get;set;}
        public string OrderBy{get;set;}

        public SearchOption(){
            this.ItemsPerPage=5;
            this.Filter="";
            this.OrderBy="";
        }
    } 
}