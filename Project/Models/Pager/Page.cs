using System;
using System.Collections.Generic;

namespace Project.Models{
    public class Page<T>{
        public int CurrentPage{get;set;}
        public int AllPages{get;set;}
        public IList<T> Items{get;set;}
    }
}