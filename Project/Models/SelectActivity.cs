﻿using System;
using System.Collections.Generic;

namespace Project.Models {
	public class SelectActivity {

		public List<Data.Activity> activity { get; set; }
		public List<bool> selected { get; set; }

		public SelectActivity() {
			activity = new List<Data.Activity>();
			selected = new List<bool>();
		}
	}
}
