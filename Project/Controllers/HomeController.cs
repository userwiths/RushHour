﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

using DataControl;
using Project.Data;

namespace Project.Controllers {
	public class HomeController : Controller {

		public ActionResult Index() {
			
			if (!HasCookie(this)) {
				return View();
			}else if (!Service.Verified(GetCookieResult(this))) {
				return View("NotVerified");
			}else if(!Service.LogIn(GetCookieResult(this))){
				RemoveCookie(this);
				return View();
			}



			Service.LogIn(GetCookieResult(this));
			if (Service.CurrentUser.IsAdmin) {
				return RedirectToAction("Index", "Admin");
			}
			return View("FullProfile", new Models.FullProfile() { appointments = Service.Appointments, user = Service.CurrentUser, activities = Service.Activities });
		}

		public ActionResult UserEdit(int id = -1) {
			DataControl.Repository<Data.User> rep = new DataControl.Repository<Data.User>();
			if (id == -1) {
				return View("EditProfile", rep.GetById(GetCookieResult(this)));
			}
			return View("EditProfile",rep.GetById(id));
		}

		public ActionResult DeleteUser(int id = -1) {
			int ids = HomeController.GetCookieResult(this);
			Repository<Data.User> rep = new Repository<Data.User>();

			var usr = rep.GetById(ids);
			if (usr == null) {
				return RedirectToAction("Index", "Home");
			} else if (id!=usr.ID && !usr.IsAdmin) {
				return RedirectToAction("Index", "Home");
			}

			if (id == -1) {
				Service.RemoveUser(GetCookieResult(this));
				RemoveCookie(this);
			} else {
				Service.RemoveUser(id);
			}

			return RedirectToAction("Index");
		}

		public ActionResult Verify(int id){
			Service.Check(id);
			return RedirectToAction("Index");
		}

		public ActionResult LogOut(){
			RemoveCookie(this);
			return RedirectToAction("Index");
		}

		public ActionResult RestorePassword(){
			return View();
		}

		public ActionResult ResendMail(){
			Service.ResendMail(GetCookieResult(this));
			return RedirectToAction("Index");
		}
		#region POST
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult RestorePassword(User usr){
			if (!ModelState.IsValid) {
				return View();
			}

			Repository<User> rep = new Repository<Data.User>();
			foreach (var item in rep.CurrentSet) {
				if(item.Name==usr.Name && item.Email==usr.Email && item.Phone==usr.Phone){
					Service.RestoreUser(item.ID,usr);
					return RedirectToAction("Index");
				}
			}
			return View("NotFound");
		}

		[HttpPost]
		//[ValidateAntiForgeryToken]
		public ActionResult LogIn(Models.Creditials usr) {
			if (!ModelState.IsValid) {
				return View("Index");
			}

			var result = Service.LogIn(usr.Name, usr.Password);
			if (result) {
				SetCookie(Service.CurrentUser.ID,this);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		//[ValidateAntiForgeryToken]
		public ActionResult SingIn(Data.User usr) {
			if (!ModelState.IsValid) {
				return View("Index");
			}

			if (!Service.LogIn(usr.Name,usr.Password)) {
				Service.AddUser(usr);
			}
			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditUser(Data.User usr,int id = -1){
			if(!ModelState.IsValid){
				return View("EditProfile"); 	
			}

			usr.IsAdmin = Service.CurrentUser.IsAdmin;
			if(id==-1){
				Service.EditUser(GetCookieResult(this),usr);
			}else{
				Service.EditUser(id,usr);
			}

			Service.LogIn(GetCookieResult(this));
			return View("FullProfile",new Models.FullProfile(){ appointments = Service.Appointments, user = Service.CurrentUser, activities = Service.Activities });
		}
#endregion

		#region COOKIE
		public static void SetCookie(int id,Controller ctr) {
			HttpCookie c = new HttpCookie("RushHour");
			c.Value = "The " + id + " Rushed";
			ctr.ControllerContext.HttpContext.Response.SetCookie(c);
		}

		public static bool HasCookie(Controller ctr) {
			return ctr.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("RushHour");
		}

		public static int GetCookieResult(Controller ctr) {
			var res = ctr.
						 ControllerContext.
						 HttpContext.
						 Request.
						 Cookies.
						 Get("RushHour").
						 Value.
						 Split(' ');
			return int.Parse(res[1]);
		}
			   
		public static void RemoveCookie(Controller ctr){
			ctr.ControllerContext.HttpContext.Response.Cookies["RushHour"].Expires=new DateTime(1800,10,10);
			
		}
		#endregion
	}
}
