﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DataControl;
using Project.Data;

namespace Project.Controllers
{
    public class ActivityController : Controller
    {
		/*<tr>
        <td>Name</td>
        <td>Duration</td>
        <td>Price</td>
        <td></td>
        <td></td>>
    </tr>
    @foreach(var e in Model.Items){
        <tr class="activity" id="@e.ID">
            <td>@e.Name</td>
            <td>@e.Duration</td>
            <td>@e.Price</td>
            <td></td>
            <td>@Html.ActionLink("Edit","EditActivity","Activity",new{id=@e.ID},new{@class="btn edit-button"})</td>
        </tr>
    } */
        public ActionResult Index(int page=0)
        {
			//Project.Controllers.Pager<Activity> pager=new Project.Controllers.Pager<Activity>(
			//	Service.Activities.AsQueryable<Activity>());
			return View (Service.Activities);
        }

		public ActionResult CreateActivity(){
			return View();
		}

		public ActionResult EditActivity(int id) {
			var act=Service.Activities.Where(y=>y.ID==id).FirstOrDefault();
			if(act==null){
				return View("Index");
			}
			return View(new Models.ReplaceActivity(){NewActivity=act,OldId=id});
		}

		public ActionResult DeleteActivity(int id) {
			Service.RemoveActivity(id);
			return RedirectToAction("Index");
		} 

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult EditActivity(Models.ReplaceActivity act){
			if(!ModelState.IsValid){
				return View(act);
			}

			Service.UpdateActivity(act.OldId,act.NewActivity);
			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult CreateActivity(Data.Activity act){
			if (!ModelState.IsValid) {
				return View(act);
			}

			Service.AddActivity(act);
			return RedirectToAction("Index");
		}
    }
}
