using System;

using System.Collections.Generic;
using System.Linq;

using Project.Data;

namespace Project.Controllers {
    public class Pager<T> where T:Data.Base,new(){
        public IQueryable<T> AllItems{get;set;}
        public IQueryable<T> CurrentItems{get;set;}
        public Models.SearchOption options{
            get{return options;}
            set{
                options=value;
                CurrentItems=AllItems;
                //ApplyOptions();
            }
        }

        public Pager(IQueryable<T> allitems,Models.SearchOption option=null){
            if(allitems.Count()!=0){
                this.AllItems=allitems;
                this.CurrentItems=allitems;
            }else{
                var ls=new List<T>();
                this.AllItems=ls.AsQueryable();
                this.CurrentItems=ls.AsQueryable();
            }

            options=option??new Models.SearchOption();
        }

        public Models.Page<T> GetPage(int page){
            Models.Page<T> result=new Models.Page<T>();

            if(CurrentItems.Count()==0){
                result.CurrentPage=1;
                result.AllPages=1;
                result.Items=new List<T>();
                return result;
            }

            page--;
            //ApplyOptions();

            result.CurrentPage=page;
            result.AllPages=AllPages(options.ItemsPerPage);

            if(Available(page,options.ItemsPerPage)){
                result.Items=CurrentItems.ToList().GetRange(page*options.ItemsPerPage,ToTake(page,options.ItemsPerPage));
            }else{
                result.Items=CurrentItems.ToList();
            }
            
            return result;
        }

        private void ApplyOptions(){
            var filter=options.Filter.Split(' ');
            var complex=filter.Where(
                x=>x.Contains(':')).
                Select(y=> new KeyValuePair<string,string>
                (y.Split(':')[0],y.Split(':')[1]))
                .ToDictionary(x=>x.Key,x=>x.Value);
            var linera=filter.Where(x=>!x.Contains(':')).ToArray();

            ApplyComplexFilter(complex);
            ApplyLinearFilter(linera);
            ApplyOrder(this.options.OrderBy);
        }

        private void ApplyLinearFilter(string[] filter){
            foreach(var z in filter){
                CurrentItems=CurrentItems.Where(x=>
                                        x.GetType().GetProperties().Where(y=>y.GetValue(x).ToString().Contains(z)).Count()!=0
                                        );
            }
        }
        private void ApplyComplexFilter(Dictionary<string,string> opt){
            foreach(var z in opt){
                CurrentItems=CurrentItems.Where(x=>
                                        x.GetType().GetProperty(z.Key)!=null
                                        ).Where(x=>x.GetType().GetProperty(z.Key).GetValue(x).ToString().Contains(z.Value));

            }
        }

        private void ApplyOrder(string order){
            if(CurrentItems.FirstOrDefault().GetType().GetProperty(order)!=null){
                CurrentItems=CurrentItems.OrderBy(x=>x.GetType().GetProperty(order));
            }
        }

        private bool Available(int page,int perpage){
            return page*perpage<CurrentItems.Count() && page*perpage>0;
        }

        private int ToTake(int page,int perpage){
            return Math.Min(perpage,CurrentItems.Count()-page*perpage);
        }

        private int AllPages(int perpage){
            return (int)Math.Ceiling((double)(CurrentItems.Count())/perpage);
        }

    }
}