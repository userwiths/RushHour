﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DataControl;

namespace Project.Controllers
{
    public class AppointmentController : Controller
    {
        public ActionResult Index(int page=1)
        {
			List<Models.SmartAppointment> smart = new List<Models.SmartAppointment>();
			Repository<Data.Appointment> rep = new Repository<Data.Appointment>();

			if(!HomeController.HasCookie(this)){
				return RedirectToAction("Index", "Home", null);	
			}else if(!Service.LogIn(HomeController.GetCookieResult(this))){
				return RedirectToAction("Index", "Home", null);
			}else if(!Service.Verified(HomeController.GetCookieResult(this))){
				return RedirectToAction("Index", "Home", null);
			}

			foreach (var item in Service.Appointments) {
				if (Service.GetActivity(item.ID).Count() == 0) {
					smart.Add(new Models.SmartAppointment(item, new List<Data.Activity>()));
					continue;
				}

				smart.Add(new Models.SmartAppointment(item,Service.GetActivity(item.ID)));
			}
			
			//Project.Controllers.Pager<Models.SmartAppointment> pager=new Project.Controllers.Pager<Models.SmartAppointment>(
			//	smart.AsQueryable<Models.SmartAppointment>()
			//);
			

			return View (smart);
        }

		public ActionResult CreateAppointment(){
			Models.FullAppointment flpl = new Models.FullAppointment();

			foreach (var item in Service.Activities) {
				flpl.selected.activity.Add(item);
				flpl.selected.selected.Add(false);
			}

			return View(flpl);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult CreateAppointment(Models.FullAppointment smpl) {
			Data.Appointment ap = new Data.Appointment();
			Repository<Data.Appointment> app = new Repository<Data.Appointment>();

			if(!ModelState.IsValid){
				smpl = new Models.FullAppointment();
				foreach (var item in Service.Activities) {
					smpl.selected.activity.Add(item);
					smpl.selected.selected.Add(false);
				}
				return View(smpl);
			}

			ap.UserId = HomeController.GetCookieResult(this);

			var temp = new DateTime();
			if(DateTime.TryParse(smpl.pair.Start,out temp) && DateTime.TryParse(smpl.pair.End,out temp)){
				ap.Start = DateTime.Parse(smpl.pair.Start);
				ap.End = DateTime.Parse(smpl.pair.End);
			}else{
				return RedirectToAction("Index");
			}
			

			Service.AddAppointment(ap);
			int id = app.GetId(ap);

			for (int i = 0; i < smpl.selected.selected.Count;i++) {
				if(smpl.selected.selected[i]){
					Service.PairAppointmentActivity(id,Service.Activities[i].ID);
				}
			}

			return RedirectToAction("Index");
		}

		public ActionResult Edit(int id) {

			return View(new Models.ReplaceAppointment(id,Service.Activities));
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Models.ReplaceAppointment ap){
			List<Data.Activity> res = new List<Data.Activity>();
			var smpl = new Models.ReplaceAppointment();
			if(!ModelState.IsValid){
				foreach (var item in Service.Activities) {
					smpl.selected.activity.Add(item);
					smpl.selected.selected.Add(false);
				}
				return View(smpl);
			}

			for (int i = 0; i < ap.selected.selected.Count(); i++) {
				if(ap.selected.selected[i]){
					res.Add(ap.selected.activity[i]);
				}
			}

			var temp = new DateTime();
			if (!DateTime.TryParse(ap.pair.Start, out temp) || !DateTime.TryParse(ap.pair.End, out temp)) {
				return RedirectToAction("Index");
			}

			Service.UpdateAppointment(
				ap.OldId,
				new Data.Appointment() {
					UserId = HomeController.GetCookieResult(this),
					Start = DateTime.Parse(ap.pair.Start),
					End = DateTime.Parse(ap.pair.End)
				},res);
			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id) {
			Service.RemoveAppointment(id);
			return RedirectToAction("Index");
		}
    }
}
