﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using DataControl;
using Project.Models;
using Project.Data;

namespace Project.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
			if(HomeController.HasCookie(this)){
				DataControl.Repository<Data.User> usr = new DataControl.Repository<Data.User>();
				Service.LogIn(HomeController.GetCookieResult(this));
				if(Service.CurrentUser.IsAdmin){
					return View(Service.CurrentUser);
				}
			}
			return RedirectToAction("Index","Home");
        }
/*@foreach(var usr in @Model.Items){
    <div id="@usr.ID">
        @Html.Partial("UserProfile",usr)
    </div>
} */
		public ActionResult AllUsers(int pg=1) {
			DataControl.Repository<Data.User> rep = new DataControl.Repository<Data.User>();
			int myid = HomeController.GetCookieResult(this);
			//Project.Controllers.Pager<Data.User> pager=new Pager<Data.User>(rep.CurrentSet.Where(x => x.ID != myid));

			return View(rep.CurrentSet.Where(x => x.ID != myid).ToList());
		}

		public ActionResult CreateUser(){
			int id = HomeController.GetCookieResult(this);
			Repository<Data.User> rep = new Repository<Data.User>();

			var usr = rep.GetById(id);
			if(usr==null){
				return RedirectToAction("Index", "Home");
			}else if(!usr.IsAdmin){
				return RedirectToAction("Index", "Home");
			}

			return View("AdminSingIn");
		}

		public ActionResult AdminEdit(int id){
			Data.User nusr = new Data.User();
			nusr.ID = id;

			return View();
		}

		public ActionResult DeleteUser(int id){
			Service.RemoveUser(id);
			return RedirectToAction("AllUsers");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult AdminEdit(Data.User usr) {
			if (!ModelState.IsValid) {
				return View(usr);
			}

			Service.EditUser(usr.ID,usr);
			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult CreateUser(Data.User usr){
			if(!ModelState.IsValid){
				return View(usr);
			}

			Service.AddUser(usr);
			return RedirectToAction("AllUsers");
		}

    }
}
