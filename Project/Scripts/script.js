﻿function ChangeWallpaper(){
            var x = document.getElementsByTagName("body")[0];
            var oldBg=x.style.backgroundImage;
            var newBgId=Math.floor(Math.random() * 13)+2;
            var newBg="/Content/background"+newBgId+".jpg";
            if(newBg == oldBg){
                ChangeWallpaper();
            }else{
                x.background=newBg;
            }
        }

        function Populate(name){
            var dest=document.getElementById(name);
            var htmlData=dest.innerHTML;
            var src=document.getElementsByClassName("user-option");

            for(var i=0;i<src.length;i++){
                htmlData+="<li><a href='"+src[i].href+"'>"+src[i].innerText+"</a></li>";
            }
            
            htmlData+="<li><input type='button' onclick='Togle()' value='Togle'/></li>";

            dest.innerHTML=htmlData;
        }

        function Togle(){
            var llist=document.getElementsByClassName("option-block")[0];
            var rdiv=document.getElementsByClassName("user-option");

            if(llist.style.display=="block"){
                llist.style.display="none";
                for(var i=0;i<rdiv.length;i++){
                    rdiv[i].style.display="inline";
                }
            }else{
                llist.style.display="block";
                for(var i=0;i<rdiv.length;i++){
                    rdiv[i].style.display="none";
                }
            }
        }

        function Init(){
            ChangeWallpaper();
            Populate("options");
            var timer=setInterval(ChangeWallpaper,5000);
        }