﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project.Data {
	public class Base{
		public int ID { get; set; }	
	}

	public class User:Base {
		[Required]
		public string Name { get; set; }

		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		public string Password { get; set; }

		[Required]
		[Phone]
		public string Phone { get; set; }

		public bool IsAdmin { get; set; }

		public User(){
		}

		public User(string name = "John Doe", string email = "john@doe.com", string password = "none", string phone = "1234567890") {
			ID = 0;
			Name = name;
			Password = password;
			Email = email;
			Phone = phone;
			IsAdmin = false;
		}

		public override bool Equals(object obj) {
			User res = obj as User;
			if(res==null){
				return false;
			}
			return Name == res.Name && Password == res.Password && Email == res.Email && Phone == res.Phone;
		}
	}

	public class Appointment:Base {
		public int UserId { get; set; }

		[Required]
		public DateTime Start { get; set; }

		[Required]
		public DateTime End { get; set; }

		public Appointment(){
			UserId = 1;
			Start =DateTime.Now;
			End =DateTime.Now;
		}

		public override bool Equals(object obj) {
			Appointment res = obj as Appointment;
			if (res == null) {
				return false;
			}
			return UserId == res.UserId && Start == res.Start && End == res.End;
		}
	}

	public class Activity:Base {
		[Required]
		public string Name { get; set; }

		[Required]
		public decimal Duration { get; set; }

		[Required]
		public decimal Price { get; set; }

		public Activity(){
			Name = "None";
			Duration = 0.0m;
			Price = 0.0m;
		}

		public override bool Equals(object obj) {
			var temp = obj as Activity;
			if(temp==null){
				return false;
			}
			return temp.Name == Name && temp.Duration == Duration && temp.Price == Price;
		}
	}
}
