﻿using System;
namespace Project.Data {
	public class NumberPair:Base {

		public int First { get; set; }
		public int Second { get; set; }

		public NumberPair() {
		}

		public override bool Equals(object obj) {
			NumberPair num = obj as NumberPair;
			if(num==null){
				return false;
			}

			return num.First == First && num.Second == Second;
		}
	}
}
