﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using MailKit;

using Project.Data;

namespace DataControl {
	public static class Service {
		public static User CurrentUser;
		public static List<Appointment> Appointments { get; set; }
		public static List<Activity> Activities { get; set; }

		public static bool LogIn(string name, string password) {
			SetUp();

			Repository<User> rep = new Repository<User>();
			string hash = GetHash(password);
			var result = rep.CurrentSet.Where(x => x.Name == name && x.Password ==hash );

			if (result.Count() == 0) {
				return false;
			}

			CurrentUser = result.First();
			LoadAppointments();
			LoadActivity();

			return true;
		}

		public static bool LogIn(int id) {
			SetUp();

			Repository<User> rep = new Repository<User>();
			var result = rep.CurrentSet.Where(x => x.ID == id);

			if (result.Count() == 0) {
				return false;
			}

			CurrentUser = result.First();
			LoadAppointments();
			LoadActivity();

			return true;
		}

		#region DEATH

		public static void AddAppointment(Appointment ap) {
			Repository<Appointment> app = new Repository<Appointment>();

			app.Insert(ap);
			ap = app.Get(ap);

			Appointments.Add(ap);
		}

		public static void RemoveActivity(int id) {
			Repository<Activity> app = new Repository<Activity>();
			var elem = app.GetById(id);
			app.Delete(elem);

			if (Activities.Contains(elem)) {
				Activities.Remove(elem);
			}
		}

		public static void RemoveAppointment(int id) {
			Repository<Appointment> app = new Repository<Appointment>();
			var elem = app.GetById(id);
			app.Delete(elem);
			if (Appointments.Contains(elem)) {
				Appointments.Remove(elem);
			}
		}

		public static void UpdateActivity(int id, Activity ap) {
			Repository<Activity> app = new Repository<Activity>();

			app.Update(id, ap);

			for (int i = 0; i < Activities.Count; i++) {
				if (Activities[i].ID == id) {
					Activities[i] = ap;
					return;
				}
			}
		}

		public static void UpdateAppointment(int id,Appointment ap,List<Activity> act) {
			Repository<Appointment> app = new Repository<Appointment>();

			app.Update(id, ap);

			ReEnterAppointmentActivity(id,act);
		}

		public static void AddUser(User usr) {
			Repository<User> app = new Repository<User>();


			usr.Password = GetHash(usr.Password);

			app.Insert(usr);
			if(!usr.IsAdmin){
				SendEmail(usr);
			}
		}

		public static void RemoveUser(int id) {
			Repository<User> app = new Repository<User>();
			app.Delete(app.GetById(id));
		}

		public static void EditUser(int id, User usr) {
			Repository<User> app = new Repository<User>();
			usr.Password = GetHash(usr.Password);
			app.Update(id, usr);
		}

		public static void RestoreUser(int id,User usr){
			Repository<User> app = new Repository<User>();
			usr.Password = GetHash(usr.Password);
			app.Update(id, usr);
			SendEmail(usr);
		}
		#endregion

		public static void PairAppointmentActivity(int appointment, int activity) {
			Repository<AppointmentActivityPair> app = new Repository<AppointmentActivityPair>();
			app.Insert(new AppointmentActivityPair() { First = appointment, Second = activity });
		}

		public static void ReEnterAppointmentActivity(int id,List<Activity> act) {
			Repository<AppointmentActivityPair> rep = new Repository<AppointmentActivityPair>();
			var old = GetActivity(id);

			foreach (var item in act) {
				if (old.Where(x => x.Equals(item)).Count() == 0) {
					rep.Insert(new AppointmentActivityPair() { First = id,Second=item.ID});
				}
			}

			foreach (var elem in old) {
				if (act.Where(x => x.Equals(elem)).Count() == 0) {
					rep.Delete(new AppointmentActivityPair() { First=id, Second=elem.ID });
				}
			}

		}

		public static void AddActivity(Activity ap) {
			Repository<Activity> app = new Repository<Activity>();

			app.Insert(ap);
			ap = app.Get(ap);

			Activities.Add(ap);
		}

		public static List<Activity> GetActivity(int id) {
			Repository<AppointmentActivityPair> rep = new Repository<AppointmentActivityPair>();
			Repository<Activity> act = new Repository<Activity>();
			var result = new List<Activity>();

			foreach (var item in rep.CurrentSet) {
				if((item as NumberPair).First==id){
					result.Add(act.GetById((item as NumberPair).Second));
				}
			}

			return result;
		}

		#region VERIFY

		public static string GetHash(string password) {
			var temp = new ASCIIEncoding();
			var sha = new System.Security.Cryptography.SHA256CryptoServiceProvider();
			var shadata = sha.ComputeHash(Encoding.ASCII.GetBytes(password));
			return temp.GetString(shadata);
		}

		public static void ResendMail(int id){
			Repository<VerifyPair> verPair = new Repository<VerifyPair>();
			Repository<User> app = new Repository<User>();

			User usr = app.GetById(id);
			int hash = 0;

			if(usr==null){
				return;
			}

			foreach (var item in verPair.CurrentSet) {
				if(item.First==usr.ID){
					hash = item.Second;
				}
			}

			//BodyBuilder body = new BodyBuilder();

			var message = new MailMessage();
			message.Body = "<html>\n" +
				"<body>\n" +
				"<h3>Welcome To RushHour</h3>" +
				"<br>\n" +
				"<h4>Please click the link bellow to activate youre account</h4>\n" +
				"<a href='http://127.0.0.1:8080/Home/Verify/" + hash + "'>Verify</a>\n" +
				"</body>\n" +
				"</html>";
			message.From = new MailAddress("stiliyan21@gmail.com");
			message.To.Add(new MailAddress(usr.Email));
			message.Subject = "Verify Account";

			message.IsBodyHtml = true;

			using (var client = new SmtpClient("smtp.mailtrap.io")) {
				client.Credentials = new System.Net.NetworkCredential("691436b254e6be", "851c92665670bf");
				client.EnableSsl = true;

				client.Send(message);
				client.Dispose();
			}
		}

		public static void Verify(int usr, int key) {
			Repository<VerifyPair> rep = new Repository<VerifyPair>();
			rep.Insert(new VerifyPair() { First = usr, Second = key });
			//rep.context.SaveChanges();
		}

		public static bool Verified(int id) {
			Repository<VerifyPair> rep = new Repository<VerifyPair>();
			return rep.CurrentSet.Where(x => x.First == id).ToList().Count() == 0;
		}

		public static void Check(int hash) {
			Repository<VerifyPair> rep = new Repository<VerifyPair>();
			var item = rep.CurrentSet.Where(x => x.Second == hash).FirstOrDefault();
			rep.Delete(item);
		}

		private static void SendEmail(User usr) {
			Repository<User> app = new Repository<User>();
			Repository<VerifyPair> ver=new Repository<VerifyPair>();
			usr = app.Get(usr);
			int hash = RandomHash();
			ver.Insert(new VerifyPair() { First = usr.ID, Second = hash });
			
			//BodyBuilder body = new BodyBuilder();

			var message = new MailMessage();
			message.Body = "<html>" +
				"<body>" +
				"<h3>Welcome To RushHour</h3>" +
				"<br>" +
				"<h4>Please click the link bellow to activate youre account</h4>" +
				"<a href='http://127.0.0.1:8080/Home/Verify/" + hash + "'>Verify</a>" +
				"</body>" +
				"</html>";
			message.From = new MailAddress("stiliyan21@gmail.com");
			message.To.Add(new MailAddress(usr.Email));
			message.Subject = "Verify Account";

			message.IsBodyHtml = true;

			using (var client = new SmtpClient("smtp.mailtrap.io")) {
				client.Credentials = new System.Net.NetworkCredential("691436b254e6be", "851c92665670bf");
				client.EnableSsl = true;

				client.Send(message);
				client.Dispose();
			}
		}

		private static int RandomHash() {
			Random rnd = new Random();
			int result = rnd.Next(1000);
			for (int i = 0; i < 10; i++) {
				result += rnd.Next(1000);
			}
			return result;
		}

		private static void LoadAppointments() {
			Repository<Appointment> app = new Repository<Appointment>();

			if (!CurrentUser.IsAdmin) {
				Appointments = app.CurrentSet.Where(x => x.UserId == CurrentUser.ID).ToList();
			}else{
				Appointments = app.CurrentSet.ToList();	
			}
		}

		private static void LoadActivity() {
			Repository<Activity> app = new Repository<Activity>();

			Activities = app.CurrentSet.ToList();
		}

		private static void SetUp() {
			Repository<User> userRep = new Repository<User>();
			if (userRep.CurrentSet.Count() == 0) {
				User usr = new User() { Name = "admin", Email = "admin@localhost.com", Password = "admin", Phone = "1234567890", IsAdmin = true };
				Service.AddUser(usr);
			}

			Activities = new List<Activity>();
			//}
			//if (Appointments == null) {
			Appointments = new List<Appointment>();
			//}
		}
#endregion
	}

}
