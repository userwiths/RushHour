﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Data.Entity;

using Project.Data;

namespace DataControl {
	public static class StartUp{
		public static void SetUp() {
			RepContext context = new RepContext();
			context.Database.ExecuteSqlCommand("create table " +
			                                   "if not exists " +
											   "Users(" +
											   "ID mediumint primary key auto_increment," +
											   "Name varchar(30)," +
											   "Email varchar(30)," +
											   "Password varchar(75)," +
											   "Phone varchar(30)," +
											   "IsAdmin tinyint);");
			
			context.Database.ExecuteSqlCommand("create table " +
											   "if not exists " +
											   "Appointments(" +
											   "ID mediumint primary key auto_increment," +
											   "UserId mediumint," +
											   "Start date," +
											   "End date," +
											   "foreign key(UserId) references Users(ID) on delete cascade);");

			context.Database.ExecuteSqlCommand("create table " +
			                                   "if not exists " +
											   "Activities(" +
											   "ID mediumint primary key auto_increment," +
											   "Name varchar(30)," +
											   "Duration float(6,2)," +
											   "Price float(15,5));");

			context.Database.ExecuteSqlCommand("create table " +
			                                   "if not exists " +
			                                   "AppointmentActivityPairs(" +
			                                   "ID mediumint primary key auto_increment," +
			                                   "First mediumint," +
			                                   "Second mediumint," +
			                                   "foreign key(First) references Appointments(ID) on delete cascade," +
			                                   "foreign key(Second) references Activities(ID) on delete cascade);");

			context.Database.ExecuteSqlCommand("create table " +
			                                   "if not exists " +
			                                   "VerifyPairs(ID mediumint primary key auto_increment," +
			                                   "First mediumint," +
			                                   "Second mediumint," +
			                                   "foreign key(First) references Users(ID) on delete cascade);");

			context.SaveChanges();
			context.Dispose();
		}
	}

	[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
	public class RepContext:DbContext {
		public RepContext():base(new MySqlConnectionStringBuilder(
			"Data Source=127.0.0.1;" +
			"database=RushHour;" +
			"uid=root;" +
			//"password=password;" +
			"SslMode=none;").ConnectionString) {
			if(!this.Database.Exists()){
				this.Database.ExecuteSqlCommand("create database if not exists RushHour;");
			}
		}

		public virtual DbSet<User> Users { get; set; }
		public virtual DbSet<Appointment> Appointments { get; set; }
		public virtual DbSet<Activity> Activities { get; set; }

		public virtual DbSet<VerifyPair> Verify { get; set; }
		public virtual DbSet<AppointmentActivityPair> AppointmentActivity { get; set; }
	}

	public class Repository<T> where T:Base,new(){
		public static RepContext context;
		public IQueryable<T> CurrentSet;

		public Repository() {
			if (context == null) {
				context = new RepContext();
				StartUp.SetUp();
			}

			if(!context.Database.Exists()){
				context.Database.Create();
			}

			CurrentSet=context.Set<T>().OfType<T>();
		}

		public void Insert(T item){
			if (Contains(item)) {
				return;
			}

			context.Entry(item).State=EntityState.Added;

			context.SaveChanges();
		}

		public void Delete(T item){
			if(!Contains(item)){
				return;	
			}
			item = Get(item);

			context.Entry(item).State=EntityState.Deleted;
			context.SaveChanges();
		}
	
		public void Update(int id,T item){
			var elem = CurrentSet.Where(x=>x.ID==id).FirstOrDefault();

			if(elem==null){
				return;	
			}
			item.ID = (elem as Base).ID;
			context.Entry(elem).CurrentValues.SetValues(item);

			context.SaveChanges();
		}

		public bool Contains(T item){
			foreach (var elem in CurrentSet) {
				if(item.Equals(elem)){
					return true;
				}
			}
			return false;
		}

		public int GetId(T item){
			foreach (var elem in CurrentSet) {
				if(item.Equals(elem)){
					var temp = elem as Base;
					return temp.ID;
				}
			}
			return -1;
		}

		public T GetById(int item){
			foreach (var elem in CurrentSet) {
				if ((elem as Base).ID==item) {
					return elem as T;
				}
			}
			return null;
		}

		public T Get(T item){
			foreach (var elem in CurrentSet) {
				if (item.Equals(elem)) {
					return elem as T;
				}
			}
			return null;
		}

		public void Save(){
			context.SaveChanges();
		}
	}
}
